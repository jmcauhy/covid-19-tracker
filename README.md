<div align="center">
    <img alt="COVID-19 Tracker" src="src/images/logo.png" />
</div>
<br/>
<div align="center">The project is currently available to be visualized [here](https://onlinecovidtracker.netlify.app/)!</div>
<hr/>
<div align="center">
    <img alt="COVID-19 Tracker" src="https://i.imgur.com/RixfpBi.jpg" />
</div>

## :clipboard: Description

This project was developed during the Covid-19 pandemic to help people keep track of the virus statistics. It consumes an external API to provide data towards the amount of infected, cured and dead people. It also colects daily statistics to present a graphic of the spread evolution.

## :computer: Technologies
This project was developed using [React](https://reactjs.org/).

## :bulb: How to contribute

- Fork this repository;
- Create a branch with your feature: `git checkout -b my-feature`;
- Commit your changes: `git commit -m 'My new feature'`;
- Push to your branch: `git push origin my-feature`.

## :scroll: License

This project is under the [MIT](https://choosealicense.com/licenses/mit/) license.