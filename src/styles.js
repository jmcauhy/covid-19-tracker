import styled from 'styled-components'
import logoImg from './images/logo.png'

export const Container = styled.div`
   max-width: 1120px;
   min-width: 300px;
   margin: 0 auto;
`

export const Img = styled.div`
   width: 75%;
   height: 82px;
   margin: 30px auto 0 auto;
   background-image: url(${logoImg});
   background-position: center;
   background-size: contain;
   background-repeat: no-repeat;
`