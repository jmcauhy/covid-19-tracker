import React, { useState, useEffect } from 'react'

import { CountryStats } from '../../services/api'

import { Container, FormContainer, Form, Select, BootstrapInput } from './styles'

const SelectCountry = ({ country, handleSelectCountry }) => {
   const [data, setData] = useState([])

   useEffect(() => {
      const loadAPI = async () => {
         const response = await CountryStats()

         setData(response)
      }

      loadAPI()
   }, [setData])

   return (
      <Container>
         <FormContainer>
            <Form>
               <Select
                  id="demo-customized-select-native"
                  value={country}
                  onChange={e => handleSelectCountry(e.target.value)}
                  input={<BootstrapInput />}
               >
                  <option value=""> Global </option>
                  {data.map(
                     (country, i) => <option key={i} value={`${country}`}> {country} </option>
                  )}
               </Select>
            </Form>
         </FormContainer>
      </Container>
   )
}

export default SelectCountry