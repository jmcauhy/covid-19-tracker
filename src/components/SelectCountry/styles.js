import styled from 'styled-components'
import { FormControl, NativeSelect, InputBase } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'

export const Container = styled.div`
   width: 100%;
   margin: 30px auto 0 auto;

   @media (min-width: 860px) {
      width: 50%;
   }
`
export const FormContainer = styled.div` margin: 0 15px; `

export const Form = styled(FormControl)` width: 100%; `

export const Select = styled(NativeSelect)`
   width: 100%;
   background-color: #FFF;
`

export const BootstrapInput = withStyles(theme => ({
   root: {
      'label + &': { marginTop: theme.spacing(3) }
   },
   input: {
      borderRadius: 8,
      position: 'relative',
      color: '#778',
      fontWeight: '500',
      backgroundColor: 'transparent',
      border: '2px solid #99a',
      fontSize: 16,
      padding: '10px 30px 10px 15px',
      transition: theme.transitions.create(['border-color', 'box-shadow']),
      fontFamily: 'Roboto',
      '&:focus': {
         borderRadius: 8,
         borderColor: '#778',
      },
   }
}))(InputBase)