import styled from 'styled-components'

export const Container = styled.div`
   width: 100%;
   margin-top: 30px;
   padding: 20px 20px 30px 20px;
   background-color: #F0F0F5;

   @media (min-width: 860px) {
      border-radius: 10px;
   }
`

export const Title = styled.h1`
   margin-bottom: 15px;
   color: #99a;
   font-size: 18px;
`