import React, { useState, useEffect } from 'react'

import { DailyStats } from '../../services/api'

import { Line, Bar } from 'react-chartjs-2'

import { Container, Title } from './styles'

const Chart = ({ stats: { confirmed, deaths, recovered }, country }) => {
   const [data, setData] = useState([])

   useEffect(() => {
      const loadAPI = async () => {
         const response = await DailyStats()

         setData(response)
      }

      loadAPI()
   }, [])

   const lineChart = (
      data.length ?
      (<Line
         height={(window.innerWidth >= 860 ? 150 : 400)}
         data={{
            labels: data.map(({ date }) => date),
            datasets: [{
               label: 'Infecções',
               data: data.map(({ confirmed }) => confirmed),
               borderColor: '#fc6',
               fill: true
            },
            {
               label: 'Mortes',
               data: data.map(({ deaths }) => deaths),
               backgroundColor: 'rgba(255,0,0,.4)',
               borderColor: '#f66',
               fill: true
            }]
         }}
      />)
      : null
   )

   const barChart = (
      confirmed ?
      (<Bar
         height={(window.innerWidth >= 860 ? 150 : 400)}
         data={{
            labels: ['Infectados', 'Curados', 'Mortos'],
            datasets: [{
               label: 'People',
               backgroundColor: ['rgba(0,0,255,.4)', 'rgba(0,255,0,.4)', 'rgba(255,0,0,.4)'],
               data: [confirmed.value, recovered.value, deaths.value]
            }]
         }}
         options={{ legend: { display: false } }}
      />)
      : null
   )

   return (
      <Container>
         <Title>{ country ? `Situação atual no(a) ${country}:`: 'Gráfico mundial:' }</Title>
         { country ? barChart : lineChart }
      </Container>
   )
}

export default Chart