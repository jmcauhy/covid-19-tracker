import styled from 'styled-components'

export const Section = styled.section`
   /* --- GRID ---*/
   display: grid;
   grid-template-columns: repeat(12, 1fr);
   /*-------------*/
   width: 100%;

   @media (min-width: 860px) {
      /* --- FLEX ---*/
      display: flex;
      /*-------------*/
      margin: 10px 0;
   }
`

export const Card2 = styled.div`
   margin: 20px 15px 0 15px;
   padding: 15px 20px;
   background-color: #FFF;
   border-radius: 10px;
   box-shadow: 0px 0px 8px 0px rgba(0,0,0,.2);
   /*-------------*/
   grid-column: 1 / -1;
`

export const Card1 = styled(Card2)`
   @media (min-width: 860px) {
      margin-left: 0;
   }
`

export const Card3 = styled(Card2)`
   @media (min-width: 860px) {
      margin-right: 0;
   }
`


export const CardHeader = styled.div`
   /* --- FLEX ---*/
   display: flex;
   align-items: center;
   justify-content: space-between;
   /*-------------*/
`

export const Title = styled.h1`
   color: #99a;
   font-size: 16px;
`

export const UpdatedOn = styled.div`
   /* --- FLEX ---*/
   display: flex;
   align-items: center;
   /*-------------*/
   color: #99a;
   font-size: 13px;
`

export const CountNumber = styled.h2`
   margin-top: 12px;
   color: #556;
   font-weight: 300;
   font-size: 34px;
`

export const CardInfo = styled.p`
   /* --- FLEX ---*/
   display: flex;
   align-items: center;
   /*-------------*/
   margin-top: 15px;
   color: #99a;
   font-size: 13px;
`

