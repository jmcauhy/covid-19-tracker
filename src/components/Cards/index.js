import React from 'react'
import CountUp from 'react-countup'

import { Section, Card1, Card2, Card3, Title, CountNumber, UpdatedOn, CardInfo, CardHeader } from './styles'

import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined'
import UpdateIcon from '@material-ui/icons/Update'

const Cards = ({ stats: { confirmed, recovered, deaths, lastUpdate }}) => {
   if (!confirmed) return 'Carregando...'

   const date = `${new Date(lastUpdate).getHours()}:${new Date(lastUpdate).getMinutes()}`

   return (
      <Section>
         <Card1 style={{ borderBottom: '5px solid #fc6' }}>
            <CardHeader>
               <Title> Infectados </Title>
               <UpdatedOn>
                  <UpdateIcon style={{ marginRight: 5, fontSize: 16 }} /> {date}
               </UpdatedOn>
            </CardHeader>

            <CountNumber><CountUp start={0} end={confirmed.value} duration={2} separator="." /></CountNumber>

            <CardInfo>
               <InfoOutlinedIcon style={{ marginRight: 5, fontSize: 22 }} />
               Este é o número de pessoas infectados de que se há notícia até o momento.
            </CardInfo>
         </Card1>

         <Card2 style={{ borderBottom: '5px solid #9f9' }}>
            <CardHeader>
               <Title> Curados </Title>
               <UpdatedOn>
                  <UpdateIcon style={{ marginRight: 5, fontSize: 16 }} /> {date}
               </UpdatedOn>
            </CardHeader>

            <CountNumber><CountUp start={0} end={recovered.value} duration={2} separator="." /></CountNumber>

            <CardInfo>
               <InfoOutlinedIcon style={{ marginRight: 5, fontSize: 22 }} />
               Este é o número de pessoas curadas de que se há notícia até o momento.
            </CardInfo>
         </Card2>

         <Card3 style={{ borderBottom: '5px solid #f66' }}>
            <CardHeader>
               <Title> Mortos </Title>
               <UpdatedOn>
                  <UpdateIcon style={{ marginRight: 5, fontSize: 16 }} /> {date}
               </UpdatedOn>
            </CardHeader>

            <CountNumber><CountUp start={0} end={deaths.value} duration={2} separator="." /></CountNumber>

            <CardInfo>
               <InfoOutlinedIcon style={{ marginRight: 5, fontSize: 22}} />
               Este é o número de pessoas falecidas de que se há notícia até o momento.
            </CardInfo>
         </Card3>
      </Section>
   )
}

export default Cards