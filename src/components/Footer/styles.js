import styled from 'styled-components'

export const Container = styled.div`
   width: 100%;
   padding: 0 10%;
   background-color: rgb(220, 220, 230);

   @media (min-width: 860px) {
      margin-top: 30px;
      padding: 0 3%;
      background-color: #F0F0F5;
      border-radius: 10px 10px 0 0;
   }
`

export const Links = styled.div`
   /* --- FLEX ---*/
   display: flex;
   align-items: center;
   justify-content: space-between;
   /*-------------*/
   padding: 25px 0 20px 0;
   color: #889;
   font-size: 14px;
`

export const Img = styled.img`
   width: 40px;
   height: 40px;
`

export const Copyright = styled.p`
padding: 10px 0 20px 0;
   color: #889;
   font-size: 14px;
`