import React from 'react'

import { Container, Links, Img, Copyright } from './styles'

import gitlabImg from '../../images/gitlab.png'

const Footer = () => {
   return (
      <Container>
         <Links>
            <a href="https://gitlab.com/jmcauhy/covid-19-tracker" rel="noopener noreferrer" target="_blank">
               <Img src={gitlabImg} />
            </a>
            <p>
               Made with ❤ by
               <strong>
                  <a
                     href="https://gitlab.com/jmcauhy"
                     rel="noopener noreferrer"
                     target="_blank"
                     style={{ color: '#99a' }}
                  >
                  &nbsp;jmcauhy
                  </a>
               </strong>
            </p>
         </Links>
         <Copyright>2020 &copy; Todos os direitos reservados.</Copyright>
      </Container>
   )
}

export default Footer