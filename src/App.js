import React, { useState, useEffect } from 'react'

import { RealTimeStats } from './services/api'

import './global.css'
import { Container, Img } from './styles'

import { Cards, Chart, SelectCountry, Footer } from './components'

const App = () => {
   const [data, setData] = useState({}),
         [country, setCountry] = useState('')

   useEffect(() => {
      const loadAPI = async () => {
         const response = await RealTimeStats()

         setData(response)
      }

      loadAPI()
   }, [])

   const handleSelectCountry = async country => {
      const response = await RealTimeStats(country)

      setData(response)
      setCountry(country)
   }

   return (
      <Container>
         <Img />
         <SelectCountry country={country} handleSelectCountry={handleSelectCountry} />
         <Cards stats={data} />
         <Chart stats={data} country={country} />
         <Footer />
      </Container>
   )
}

export default App